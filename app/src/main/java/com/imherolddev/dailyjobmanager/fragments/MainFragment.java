package com.imherolddev.dailyjobmanager.fragments;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.imherolddev.dailyjobmanager.R;
import com.imherolddev.dailyjobmanager.adapters.MainFragmentAdapter;
import com.melnykov.fab.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * By imherolddev, on 11/16/2014
 */
public class MainFragment extends Fragment {

    public static final String TAG = "MainFragment";

    @InjectView(R.id.recyclerView) RecyclerView recyclerView;
    @InjectView(R.id.fab) FloatingActionButton fab;

    private FloatingActionMenu actionMenu;
    private SubActionButton btnClockIn;
    private SubActionButton btnNewLog;

    private MainFragmentAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        ButterKnife.inject(this, rootView);

        initRecyclerView();
        initFAB();

        return rootView;

    }

    private void initRecyclerView() {

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new MainFragmentAdapter();
        recyclerView.setAdapter(adapter);

    }

    private void initFAB() {

        Activity activity = getActivity();

        fab.attachToRecyclerView(recyclerView);

        SubActionButton.Builder builder = new SubActionButton.Builder(activity);

        Drawable selector = getResources().getDrawable(R.drawable.fab_menu_item_selector);

        ImageView ic_btnClockIn = new ImageView(activity);
        ic_btnClockIn.setImageDrawable(getResources().getDrawable(R.drawable.ic_access_time_white_24dp));
        btnClockIn = builder.setContentView(ic_btnClockIn)
                .setBackgroundDrawable(selector).build();

        btnClockIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionMenu.close(true);
            }
        });

        ImageView ic_btnNewLog = new ImageView(activity);
        ic_btnNewLog.setImageDrawable(getResources().getDrawable(R.drawable.ic_note_add_white_24dp));
        btnNewLog = builder.setContentView(ic_btnNewLog)
                .setBackgroundDrawable(selector).build();

        btnNewLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionMenu.close(true);
            }
        });

        actionMenu = new FloatingActionMenu.Builder(activity)
                .addSubActionView(btnClockIn)
                .addSubActionView(btnNewLog)
                .attachTo(fab)
                .build();

    }

}
