package com.imherolddev.dailyjobmanager.fragments;
/*
 * Contributors:
 * Jason Hall - imherolddev
 *
 * Jan 02, 2015
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.imherolddev.dailyjobmanager.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Class Description
 */
public class NewLogFragment extends Fragment {

    public static final String TAG = "NewLogFragment";

    @InjectView(R.id.et_job_name) AutoCompleteTextView tv_job;
    @InjectView(R.id.et_log_title) EditText tv_title;
    @InjectView(R.id.et_log_entry) EditText tv_entry;

    public static NewLogFragment newInstance() {
        return new NewLogFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_new_log, container, false);

        ButterKnife.inject(this, rootView);

        return rootView;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
