package com.imherolddev.dailyjobmanager.listeners;

import com.imherolddev.dailyjobmanager.models.ClockTime;
import com.imherolddev.dailyjobmanager.models.DailyLog;
import com.imherolddev.dailyjobmanager.models.Job;

import java.util.ArrayList;

/**
 * By imherolddev, on 11/20/14
 */
public class ListenerUtility {

    public interface JobListListener {
        public ArrayList<Job> getJobList();
    }

    public interface LogListListener {
        public ArrayList<DailyLog> getLogList();
        public ArrayList<DailyLog> getLogList(int[] jobs);
        public ArrayList<DailyLog> getLogList(long[] dates);
    }

    public interface ClockListListener {
        public ArrayList<ClockTime> getClockList();
        public ArrayList<ClockTime> getClockList(int[] jobs);
        public ArrayList<ClockTime> getClockList(long[] dates);
    }

}
