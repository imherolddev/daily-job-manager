package com.imherolddev.dailyjobmanager.models;

/**
 * By imherolddev, on 11/15/2014
 */
public class DailyLog {

    /**
     * Unique identifier
     */
    private long _id;
    /**
     * Time in millis of creation
     */
    private long creation;
    /**
     * Associated Job unique identifier
     */
    private long jobId;
    /**
     * Log title text
     */
    private String title;
    /**
     * Log entry text
     */
    private String entry;

    /**
     * Constructor with all parameters
     */
    public DailyLog(long creation, long jobId, String title, String entry) {

        this.setCreation(creation);
        this.setJobId(jobId);
        this.setTitle(title);
        this.setEntry(entry);

    }

    public long get_id() {
        return _id;
    }

    private void set_id(long _id) {
        this._id = _id;
    }

    public long getCreation() {
        return creation;
    }

    public void setCreation(long creation) {
        this.creation = creation;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

}
