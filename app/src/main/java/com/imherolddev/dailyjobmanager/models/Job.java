package com.imherolddev.dailyjobmanager.models;

/**
 * By imherolddev, on 11/15/2014
 */
public class Job {

    /**
     * Unique identifier
     */
    private long _id;
    /**
     * Job name
     */
    private String jobName;

    public Job(long _id, String jobName) {

        this.set_id(_id);
        this.setJobName(jobName);

    }

    public long get_id() {
        return _id;
    }

    private void set_id(long _id) {
        this._id = _id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

}
