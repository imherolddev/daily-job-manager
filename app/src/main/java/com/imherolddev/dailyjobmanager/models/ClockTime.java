package com.imherolddev.dailyjobmanager.models;

/**
 * By imherolddev, on 11/16/2014
 */
public class ClockTime {

    /**
     * Unique identifier
     */
    private long _id;
    /**
     * Job unique identifier
     */
    private long jobId;
    /**
     * Clock time in millis
     */
    private long time;

    public ClockTime(long _id, long jobId, long time) {

        this.set_id(_id);
        this.setJobId(jobId);
        this.setTime(time);

    }

    public long get_id() {
        return _id;
    }

    private void set_id(long _id) {
        this._id = _id;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
