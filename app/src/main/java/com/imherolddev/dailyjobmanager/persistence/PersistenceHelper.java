package com.imherolddev.dailyjobmanager.persistence;

/**
 * By imherolddev, on 11/15/2014
 */
public class PersistenceHelper {

    public static final String DB_NAME = "job_manager_db";



    public static final String JOBS_TBL = "jobs";
    public static final String JOB_ID_COL = "_id";
    public static final String JOB_NAME_COL = "jobName";

    public static final String DAILYLOGS_TBL = "logs";
    public static final String DAILYLOGS_ID_COL = "_id";
    public static final String DAILYLOGS_CREATION_COL = "creation";
    public static final String DAILYLOGS_JOBID_COL = "jobId";
    public static final String DAILYLOGS_TITLE_COL = "title";
    public static final String DAILYLOGS_ENTRY_COL = "entry";

    public static final String CLOCKTIMES_TBL = "clocktimes";
    public static final String CLOCKTIMES_ID_COL = "_id";
    public static final String CLOCKTIMES_JOBID_COL = "jobId";
    public static final String CLOCKTIMES_TIME_COL = "time";

}
